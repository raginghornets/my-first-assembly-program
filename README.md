# My First Assembly Program

## Description

An assembly program that does nothing.

## Objective

Make a program using Assembly Language and [NASM](https://www.nasm.us/).

## Motivation

Assembly language seems fun.

## Setup (Windows)

1. [Download NASM](https://www.nasm.us/pub/nasm/releasebuilds/?C=M;O=D).
2. Add NASM to your environment variables.
3. To compile the code, open Command Prompt or Git Bash and type `nasm -f elf main.asm`.
5. To link the `.o` file to an `.exe` file, type `ld main.o`.
6. Finally, to run the program, if you are in Command Prompt, you can type `start a`. If you are in Git Bash, you can type `./a`.

## Notes

- You can also use [FASM](https://flatassembler.net/) instead of NASM.